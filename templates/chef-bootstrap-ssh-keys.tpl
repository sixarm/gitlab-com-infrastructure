#!/bin/sh
set -eux

cd "${chef_repo_dir}"

bundle exec knife bootstrap "${ssh_user}@${ip_address}" \
  --ssh-identity-file "${ssh_private_key}" \
  --no-host-key-verify \
  --sudo \
  --environment "${environment == "prod" ? "prd" : environment}" \
  --node-name "${hostname}" \
  --bootstrap-version "${chef_version}" \
  --run-list 'role[gitlab]' \
  --json-attributes "{\"azure\":{\"ipaddress\":\"${ip_address}\"}}" \
  --bootstrap-vault-json '${chef_vaults}' \
  --yes

bundle exec knife node from file "nodes/${hostname}.json"
