variable "chef_repo_dir" {}
variable "chef_vaults" {}

variable "chef_vault_env" {
  default = "_default"
}

variable "chef_version" {}
variable "domain_name" {}
variable "environment" {}
variable "gitlab_com_zone_id" {}
variable "gitlab_net_zone_id" {}
variable "location" {}
variable "count" {}
variable "instance_type" {}
variable "name" {}
variable "resource_group_name" {}
variable "ssh_private_key" {}
variable "ssh_public_key" {}
variable "ssh_user" {}
variable "subnet_id" {}
variable "tier" {}
variable "use_dns" {}
variable "use_public_ip" {}
variable "public_name" {}
variable "use_tier_in_suffix" {}
