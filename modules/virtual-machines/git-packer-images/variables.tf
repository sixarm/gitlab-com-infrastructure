variable "address_prefix" {}
variable "chef_repo_dir" {}
variable "chef_vaults" {}

variable "chef_vault_env" {
  default = "_default"
}

variable "chef_version" {}
variable "count" {}
variable "environment" {}
variable "gitlab_com_zone_id" {}
variable "instance_type" {}
variable "location" {}
variable "resource_group_name" {}
variable "source_image" {}
variable "ssh_key" {}
variable "ssh_user" {}
variable "subnet_id" {}
variable "tier" {}
