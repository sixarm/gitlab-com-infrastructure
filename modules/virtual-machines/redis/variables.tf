variable "address_prefix" {}
variable "chef_repo_dir" {}
variable "chef_vaults" {}

variable "chef_vault_env" {
  default = "_default"
}

variable "chef_version" {}
variable "environment" {}
variable "gitlab_com_zone_id" {}
variable "ip_skew" {}
variable "location" {}
variable "persistence" {}
variable "redis_count" {}
variable "redis_instance_type" {}
variable "resource_group_name" {}
variable "sentinel_count" {}
variable "sentinel_datadisk_size" {}
variable "sentinel_instance_type" {}
variable "ssh_private_key" {}
variable "ssh_public_key" {}
variable "ssh_user" {}
variable "subnet_id" {}
variable "tier" {}
