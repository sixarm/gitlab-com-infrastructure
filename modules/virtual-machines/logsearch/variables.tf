variable "address_prefix" {}
variable "chef_repo_dir" {}
variable "chef_vaults" {}

variable "chef_vault_env" {
  default = "_default"
}

variable "chef_version" {}
variable "environment" {}
variable "gitlab_net_zone_id" {}
variable "location" {}
variable "logsearch_client_count" {}
variable "logsearch_client_instance_type" {}
variable "logsearch_datanode_count" {}
variable "logsearch_datanode_instance_type" {}
variable "logsearch_master_count" {}
variable "logsearch_master_instance_type" {}
variable "resource_group_name" {}
variable "ssh_private_key" {}
variable "ssh_public_key" {}
variable "ssh_user" {}
variable "subnet_id" {}
variable "tier" {}
