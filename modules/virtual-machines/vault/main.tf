resource "azurerm_availability_set" "vault" {
  name                         = "${format("vault-%v", var.environment)}"
  location                     = "${var.location}"
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

resource "azurerm_network_interface" "vault" {
  count                   = "${var.count}"
  name                    = "${format("vault-%02d-%v-%v", count.index + 1, var.tier, var.environment)}"
  internal_dns_name_label = "${format("vault-%02d-%v-%v", count.index + 1, var.tier, var.environment)}"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "${format("vault-%02d-%v", count.index + 1, var.environment)}"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "${join(".", slice(split(".", var.address_prefix), 0, 3))}.${count.index + 101}"
  }
}

resource "aws_route53_record" "vault" {
  count   = "${var.count}"
  zone_id = "${var.gitlab_net_zone_id}"
  name    = "${format("vault-%02d.%v.%v.gitlab.net.", count.index + 1, var.tier, var.environment)}"
  type    = "A"
  ttl     = "300"
  records = ["${azurerm_network_interface.vault.*.private_ip_address[count.index]}"]
}
