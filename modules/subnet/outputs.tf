output "subnet_id" {
  value = "${azurerm_subnet.subnet.id}"
}

output "address_prefix" {
  value = "${azurerm_subnet.subnet.address_prefix}"
}

output "resource_group_name" {
  value = "${var.resource_group_name}"
}

output "resource_group_id" {
  value = "${azurerm_resource_group.resource_group.id}"
}

output "security_group_name" {
  value = "${azurerm_network_security_group.security_group.name}"
}

output "security_group_id" {
  value = "${azurerm_network_security_group.security_group.id}"
}
