#!/bin/bash
set -ex
exec &> >(tee -a "/tmp/bootstrap.log")

AWS_ACCESS_KEY=$1
AWS_SECRET_KEY=$2

useradd -u 1100 git # this uid is used for the restore
export DEBIAN_FRONTEND=noninteractive

mkdir -p /var/opt/gitlab
mount /dev/gitlab_vg/gitlab_var /var/opt/gitlab

# Set apt config, update repos and disable postfix prompt
curl https://packages.gitlab.com/install/repositories/gitlab/nightly-builds/script.deb.sh | sudo bash
debconf-set-selections <<< "postfix postfix/main_mailer_type string 'No configuration'"

# install everything in one go
apt-get -y install daemontools lzop gcc make python3 virtualenv python3-dev libssl-dev gitlab-ee ca-certificates postfix
gitlab-ctl reconfigure

# stop postgres just after reconfig
gitlab-ctl stop postgresql

sed -i 's/^max_replication_slots = 0/max_replication_slots = 100/' /var/opt/gitlab/postgresql/data/postgresql.conf

# Configure wal-e
mkdir -p /opt/wal-e /etc/wal-e.d/env
virtualenv --python=python3 /opt/wal-e
/opt/wal-e/bin/pip3 install boto azure wal-e

echo "$AWS_ACCESS_KEY" > /etc/wal-e.d/env/AWS_ACCESS_KEY_ID
echo "$AWS_SECRET_KEY" > /etc/wal-e.d/env/AWS_SECRET_ACCESS_KEY
echo 's3://gitlab-dbprod-backups/db1' > /etc/wal-e.d/env/WALE_S3_PREFIX
chmod 600 /etc/wal-e.d/env/AWS_SECRET_ACCESS_KEY
chown gitlab-psql /etc/wal-e.d/env/AWS_SECRET_ACCESS_KEY
echo 'us-east-1' > /etc/wal-e.d/env/AWS_REGION

# most recent backup
last_backup_line=$(/usr/bin/envdir /etc/wal-e.d/env /opt/wal-e/bin/wal-e backup-list 2>/dev/null | tail -1)
last_backup=$(echo "$last_backup_line" | cut -f1)
last_backup_date=$(echo "$last_backup_line" | cut -f2 | sed -e 's/T.*/ 05:30:00/')

# pre-create recovery.conf
cat > /var/opt/gitlab/postgresql/data/recovery.conf.create <<RECOVERY
restore_command = '/usr/bin/envdir /etc/wal-e.d/env /opt/wal-e/bin/wal-e wal-fetch "%f" "%p"'
recovery_target_action = 'promote'
recovery_target_time = '$last_backup_date'
RECOVERY
chown gitlab-psql:gitlab-psql /var/opt/gitlab/postgresql/data/recovery.conf.create

# create a db-restore script
cat > /tmp/start-restore.sh <<RESTORE
#!/usr/bin/env bash
gitlab-ctl stop postgresql
cp /var/opt/gitlab/postgresql/data/recovery.conf.create /var/opt/gitlab/postgresql/data/recovery.conf
/usr/bin/envdir /etc/wal-e.d/env /opt/wal-e/bin/wal-e backup-fetch /var/opt/gitlab/postgresql/data  "$last_backup"
gitlab-ctl start posgresql
RESTORE

# don't do migrations as part of the package installation
touch /etc/gitlab/skip-auto-migrations

# create a script for enabling auto nightly updates
cat > /etc/cron.daily/update-gitlab-nightly <<NIGHTLY
#!/bin/bash -e
exec &> >(tee -a "/tmp/update-nightly.log")
echo "Starting GitLab automatic upgrade - $(date)"
set -e
if grep down <(gitlab-ctl status); then
    echo "Gitlab is not running, not upgrading"
    exit 1
fi
apt-get update
apt-get install -y gitlab-ee
gitlab-rake db:migrate
gitlab-ctl reconfigure
sed -i 's/^max_replication_slots = 0/max_replication_slots = 100/' /var/opt/gitlab/postgresql/data/postgresql.conf
gitlab-ctl restart
NIGHTLY
chmod 755 /tmp/start-restore.sh /etc/cron.daily/update-gitlab-nightly
