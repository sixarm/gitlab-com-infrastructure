###########################################################
# This is specific to the dr environment
# and defines the mapping from monitoring hosts to backend
# services

resource "google_compute_url_map" "monitoring-lb" {
  name            = "${format("%v-monitoring-lb", var.environment)}"
  default_service = "${module.prometheus.google_compute_backend_service_self_link}"

  host_rule {
    hosts        = ["prometheus.dr.gitlab.net"]
    path_matcher = "prometheus"
  }

  path_matcher {
    name            = "prometheus"
    default_service = "${module.prometheus.google_compute_backend_service_self_link}"

    path_rule {
      paths   = ["/*"]
      service = "${module.prometheus.google_compute_backend_service_self_link}"
    }
  }

  ###################################

  host_rule {
    hosts        = ["prometheus-app.dr.gitlab.net"]
    path_matcher = "prometheus-app"
  }
  path_matcher {
    name            = "prometheus-app"
    default_service = "${module.prometheus-app.google_compute_backend_service_self_link}"

    path_rule {
      paths   = ["/*"]
      service = "${module.prometheus-app.google_compute_backend_service_self_link}"
    }
  }
}
