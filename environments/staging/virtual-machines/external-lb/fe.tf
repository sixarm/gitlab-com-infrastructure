resource "azurerm_availability_set" "FrontEndLBStaging" {
  name                         = "FrontEndLBStaging"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

resource "azurerm_public_ip" "fe01" {
  name                         = "fe01-stg-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "fe01-stg"
}

resource "azurerm_network_interface" "fe01" {
  name                    = "fe01-stg"
  internal_dns_name_label = "fe01-stg"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "fe01-stg"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.128.1.101"
    public_ip_address_id          = "${azurerm_public_ip.fe01.id}"
  }
}

resource "aws_route53_record" "fe01" {
  zone_id = "${var.gitlab_com_zone_id}"
  name    = "fe01.stg.gitlab.com"
  type    = "CNAME"
  ttl     = "300"
  records = ["${azurerm_public_ip.fe01.fqdn}."]
}

data "template_file" "chef-bootstrap-fe01" {
  template = "${file("${path.root}/templates/chef-bootstrap.tpl")}"

  vars {
    ip_address          = "${azurerm_public_ip.fe01.ip_address}"
    hostname            = "fe01.stg.gitlab.com"
    chef_repo_dir       = "${var.chef_repo_dir}"
    first_user_username = "${var.first_user_username}"
    first_user_password = "${var.first_user_password}"
    chef_vaults         = "${var.chef_vaults}"
    chef_vault_env      = "${var.chef_vault_env}"
  }
}

resource "azurerm_virtual_machine" "fe01" {
  name                          = "fe01.stg.gitlab.com"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  availability_set_id           = "${azurerm_availability_set.FrontEndLBStaging.id}"
  network_interface_ids         = ["${azurerm_network_interface.fe01.id}"]
  primary_network_interface_id  = "${azurerm_network_interface.fe01.id}"
  vm_size                       = "Standard_A1_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-fe01-stg"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "fe01.stg.gitlab.com"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  provisioner "local-exec" {
    command = "${data.template_file.chef-bootstrap-fe01.rendered}"
  }

  provisioner "remote-exec" {
    inline = ["nohup bash -c 'sudo chef-client &'"]

    connection {
      type     = "ssh"
      host     = "${azurerm_public_ip.fe01.ip_address}"
      user     = "${var.first_user_username}"
      password = "${var.first_user_password}"
      timeout  = "10s"
    }
  }
}
