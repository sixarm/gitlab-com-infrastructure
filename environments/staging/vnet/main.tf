variable "location" {
  description = "The location"
}

variable "virtual_network_cidr" {
  description = "The CIDR of the virtual network"
}

resource "azurerm_resource_group" "GitLabStaging" {
  name     = "GitLabStaging"
  location = "${var.location}"
}

resource "azurerm_virtual_network" "GitLabStaging" {
  name                = "GitLabStaging"
  address_space       = ["${var.virtual_network_cidr}"]
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.GitLabStaging.name}"
}

resource "azurerm_network_security_group" "gitlab-staging-nsg" {
  name                = "gitlab-staging-nsg"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.GitLabStaging.name}"

  security_rule {
    name                       = "default-allow-ssh"
    priority                   = 1000
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "TCP"
    source_port_range          = "*"
    source_address_prefix      = "*"
    destination_port_range     = "22"
    destination_address_prefix = "*"
  }
}

output "id" {
  value = "${azurerm_virtual_network.GitLabStaging.id}"
}

output "name" {
  value = "${azurerm_virtual_network.GitLabStaging.name}"
}

output "security_group_id" {
  value = "${azurerm_network_security_group.gitlab-staging-nsg.id}"
}

output "resource_group_name" {
  value = "${azurerm_resource_group.GitLabStaging.name}"
}

output "virtual_network_cidr" {
  value = "${var.virtual_network_cidr}"
}
