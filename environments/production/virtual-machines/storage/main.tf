variable "location" {
  description = "The location"
}

variable "resource_group_name" {
  description = "The name of the resource group"
}

variable "subnet_id" {}
variable "first_user_username" {}
variable "first_user_password" {}
variable "gitlab_com_zone_id" {}

resource "azurerm_availability_set" "StorageProd" {
  name                         = "StorageProd"
  location                     = "${var.location}"
  managed                      = true
  platform_update_domain_count = 20
  platform_fault_domain_count  = 3
  resource_group_name          = "${var.resource_group_name}"
}

output "availability_set_id" {
  value = "${azurerm_availability_set.StorageProd.id}"
}

### nfs-file-01
resource "azurerm_public_ip" "nfs-file-01-public-ip1" {
  name                         = "nfs-file-01-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-01"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-01" {
  name                    = "nfs-file-01"
  internal_dns_name_label = "nfs-file-01"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-01-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.101"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-01-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-01-datadisk-0" {
  name                 = "file-01-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-1" {
  name                 = "file-01-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-2" {
  name                 = "file-01-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-3" {
  name                 = "file-01-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-4" {
  name                 = "file-01-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-5" {
  name                 = "file-01-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-6" {
  name                 = "file-01-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-7" {
  name                 = "file-01-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-8" {
  name                 = "file-01-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-9" {
  name                 = "file-01-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-10" {
  name                 = "file-01-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-11" {
  name                 = "file-01-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-12" {
  name                 = "file-01-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-13" {
  name                 = "file-01-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-14" {
  name                 = "file-01-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-01-datadisk-15" {
  name                 = "file-01-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-01" {
  name                          = "nfs-file-01"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-01.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-01"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-01"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-0.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-01-datadisk-0.disk_size_gb}"
    create_option = "Attach"
    lun           = 0
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-1.name}"
    create_option = "Attach"
    lun           = 1
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-2.name}"
    create_option = "Attach"
    lun           = 2
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-3.name}"
    create_option = "Attach"
    lun           = 3
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-4.name}"
    create_option = "Attach"
    lun           = 4
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-5.name}"
    create_option = "Attach"
    lun           = 5
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-6.name}"
    create_option = "Attach"
    lun           = 6
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-7.name}"
    create_option = "Attach"
    lun           = 7
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-8.name}"
    create_option = "Attach"
    lun           = 8
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-9.name}"
    create_option = "Attach"
    lun           = 9
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-10.name}"
    create_option = "Attach"
    lun           = 10
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-11.name}"
    create_option = "Attach"
    lun           = 11
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-12.name}"
    create_option = "Attach"
    lun           = 12
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-13.name}"
    create_option = "Attach"
    lun           = 13
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-14.name}"
    create_option = "Attach"
    lun           = 14
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-01-datadisk-15.name}"
    create_option = "Attach"
    lun           = 15
    caching       = "ReadWrite"
  }
}

resource "azurerm_public_ip" "nfs-file-02-public-ip1" {
  name                         = "nfs-file-02-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-02"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-02" {
  name                    = "nfs-file-02"
  internal_dns_name_label = "nfs-file-02"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-02-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.102"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-02-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-02-datadisk-0" {
  name                 = "file-02-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-1" {
  name                 = "file-02-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-2" {
  name                 = "file-02-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-3" {
  name                 = "file-02-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-4" {
  name                 = "file-02-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-5" {
  name                 = "file-02-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-6" {
  name                 = "file-02-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-7" {
  name                 = "file-02-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-8" {
  name                 = "file-02-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-9" {
  name                 = "file-02-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-10" {
  name                 = "file-02-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-11" {
  name                 = "file-02-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-12" {
  name                 = "file-02-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-13" {
  name                 = "file-02-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-14" {
  name                 = "file-02-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-02-datadisk-15" {
  name                 = "file-02-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-02" {
  name                          = "nfs-file-02"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-02.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-02"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-02"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-0.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-0.disk_size_gb}"
    create_option = "Attach"
    lun           = 0
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-1.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-1.disk_size_gb}"
    create_option = "Attach"
    lun           = 1
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-2.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-2.disk_size_gb}"
    create_option = "Attach"
    lun           = 2
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-3.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-3.disk_size_gb}"
    create_option = "Attach"
    lun           = 3
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-4.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-4.disk_size_gb}"
    create_option = "Attach"
    lun           = 4
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-5.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-5.disk_size_gb}"
    create_option = "Attach"
    lun           = 5
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-6.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-6.disk_size_gb}"
    create_option = "Attach"
    lun           = 6
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-7.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-7.disk_size_gb}"
    create_option = "Attach"
    lun           = 7
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-8.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-8.disk_size_gb}"
    create_option = "Attach"
    lun           = 8
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-9.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-9.disk_size_gb}"
    create_option = "Attach"
    lun           = 9
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-10.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-10.disk_size_gb}"
    create_option = "Attach"
    lun           = 10
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-11.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-11.disk_size_gb}"
    create_option = "Attach"
    lun           = 11
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-12.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-12.disk_size_gb}"
    create_option = "Attach"
    lun           = 12
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-13.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-13.disk_size_gb}"
    create_option = "Attach"
    lun           = 13
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-14.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-14.disk_size_gb}"
    create_option = "Attach"
    lun           = 14
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-02-datadisk-15.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-02-datadisk-15.disk_size_gb}"
    create_option = "Attach"
    lun           = 15
    caching       = "ReadWrite"
  }
}

### nfs-file-03
resource "azurerm_public_ip" "nfs-file-03-public-ip1" {
  name                         = "nfs-file-03-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-03"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-03" {
  name                    = "nfs-file-03"
  internal_dns_name_label = "nfs-file-03"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-03-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.103"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-03-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-03-datadisk-0" {
  name                 = "file-03-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-1" {
  name                 = "file-03-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-2" {
  name                 = "file-03-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-3" {
  name                 = "file-03-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-4" {
  name                 = "file-03-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-5" {
  name                 = "file-03-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-6" {
  name                 = "file-03-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-7" {
  name                 = "file-03-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-8" {
  name                 = "file-03-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-9" {
  name                 = "file-03-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-10" {
  name                 = "file-03-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-11" {
  name                 = "file-03-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-12" {
  name                 = "file-03-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-13" {
  name                 = "file-03-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-14" {
  name                 = "file-03-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-03-datadisk-15" {
  name                 = "file-03-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-03" {
  name                          = "nfs-file-03"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-03.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-03"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-03"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-0.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-0.disk_size_gb}"
    create_option = "Attach"
    lun           = 0
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-1.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-1.disk_size_gb}"
    create_option = "Attach"
    lun           = 1
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-3.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-3.disk_size_gb}"
    create_option = "Attach"
    lun           = 3
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-4.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-4.disk_size_gb}"
    create_option = "Attach"
    lun           = 4
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-2.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-2.disk_size_gb}"
    create_option = "Attach"
    lun           = 2
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-5.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-5.disk_size_gb}"
    create_option = "Attach"
    lun           = 5
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-6.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-6.disk_size_gb}"
    create_option = "Attach"
    lun           = 6
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-7.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-7.disk_size_gb}"
    create_option = "Attach"
    lun           = 7
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-8.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-8.disk_size_gb}"
    create_option = "Attach"
    lun           = 8
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-9.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-9.disk_size_gb}"
    create_option = "Attach"
    lun           = 9
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-10.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-10.disk_size_gb}"
    create_option = "Attach"
    lun           = 10
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-11.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-11.disk_size_gb}"
    create_option = "Attach"
    lun           = 11
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-12.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-12.disk_size_gb}"
    create_option = "Attach"
    lun           = 12
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-13.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-13.disk_size_gb}"
    create_option = "Attach"
    lun           = 13
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-14.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-14.disk_size_gb}"
    create_option = "Attach"
    lun           = 14
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-03-datadisk-15.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-03-datadisk-15.disk_size_gb}"
    create_option = "Attach"
    lun           = 15
    caching       = "ReadWrite"
  }
}

### nfs-file-04
resource "azurerm_public_ip" "nfs-file-04-public-ip1" {
  name                         = "nfs-file-04-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-04"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-04" {
  name                    = "nfs-file-04"
  internal_dns_name_label = "nfs-file-04"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-04-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.104"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-04-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-04-datadisk-0" {
  name                 = "file-04-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-1" {
  name                 = "file-04-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-2" {
  name                 = "file-04-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-3" {
  name                 = "file-04-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-4" {
  name                 = "file-04-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-5" {
  name                 = "file-04-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-6" {
  name                 = "file-04-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-7" {
  name                 = "file-04-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-8" {
  name                 = "file-04-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-9" {
  name                 = "file-04-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-10" {
  name                 = "file-04-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-11" {
  name                 = "file-04-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-12" {
  name                 = "file-04-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-13" {
  name                 = "file-04-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-14" {
  name                 = "file-04-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-04-datadisk-15" {
  name                 = "file-04-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-04" {
  name                          = "nfs-file-04"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-04.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-04"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-04"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-0.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-0.disk_size_gb}"
    create_option = "Attach"
    lun           = 0
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-1.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-1.disk_size_gb}"
    create_option = "Attach"
    lun           = 1
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-2.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-2.disk_size_gb}"
    create_option = "Attach"
    lun           = 2
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-3.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-3.disk_size_gb}"
    create_option = "Attach"
    lun           = 3
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-4.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-4.disk_size_gb}"
    create_option = "Attach"
    lun           = 4
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-5.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-5.disk_size_gb}"
    create_option = "Attach"
    lun           = 5
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-6.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-6.disk_size_gb}"
    create_option = "Attach"
    lun           = 6
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-7.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-7.disk_size_gb}"
    create_option = "Attach"
    lun           = 7
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-8.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-8.disk_size_gb}"
    create_option = "Attach"
    lun           = 8
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-9.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-9.disk_size_gb}"
    create_option = "Attach"
    lun           = 9
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-10.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-10.disk_size_gb}"
    create_option = "Attach"
    lun           = 10
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-11.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-11.disk_size_gb}"
    create_option = "Attach"
    lun           = 11
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-12.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-12.disk_size_gb}"
    create_option = "Attach"
    lun           = 12
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-13.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-13.disk_size_gb}"
    create_option = "Attach"
    lun           = 13
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-14.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-14.disk_size_gb}"
    create_option = "Attach"
    lun           = 14
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-04-datadisk-15.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-04-datadisk-15.disk_size_gb}"
    create_option = "Attach"
    lun           = 15
    caching       = "ReadWrite"
  }
}

### nfs-file-05
resource "azurerm_public_ip" "nfs-file-05-public-ip1" {
  name                         = "nfs-file-05-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-05"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-05" {
  name                    = "nfs-file-05"
  internal_dns_name_label = "nfs-file-05"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-05-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.105"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-05-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-05-datadisk-0" {
  name                 = "file-05-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-1" {
  name                 = "file-05-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-2" {
  name                 = "file-05-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-3" {
  name                 = "file-05-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-4" {
  name                 = "file-05-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-5" {
  name                 = "file-05-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-6" {
  name                 = "file-05-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-7" {
  name                 = "file-05-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-8" {
  name                 = "file-05-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-9" {
  name                 = "file-05-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-10" {
  name                 = "file-05-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-11" {
  name                 = "file-05-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-12" {
  name                 = "file-05-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-13" {
  name                 = "file-05-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-14" {
  name                 = "file-05-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-05-datadisk-15" {
  name                 = "file-05-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-05" {
  name                          = "nfs-file-05"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-05.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-05"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-05"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-0.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-0.disk_size_gb}"
    create_option = "Attach"
    lun           = 0
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-1.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-1.disk_size_gb}"
    create_option = "Attach"
    lun           = 1
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-2.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-2.disk_size_gb}"
    create_option = "Attach"
    lun           = 2
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-3.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-3.disk_size_gb}"
    create_option = "Attach"
    lun           = 3
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-4.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-4.disk_size_gb}"
    create_option = "Attach"
    lun           = 4
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-5.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-5.disk_size_gb}"
    create_option = "Attach"
    lun           = 5
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-6.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-6.disk_size_gb}"
    create_option = "Attach"
    lun           = 6
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-7.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-7.disk_size_gb}"
    create_option = "Attach"
    lun           = 7
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-8.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-8.disk_size_gb}"
    create_option = "Attach"
    lun           = 8
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-9.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-9.disk_size_gb}"
    create_option = "Attach"
    lun           = 9
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-10.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-10.disk_size_gb}"
    create_option = "Attach"
    lun           = 10
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-11.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-11.disk_size_gb}"
    create_option = "Attach"
    lun           = 11
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-12.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-12.disk_size_gb}"
    create_option = "Attach"
    lun           = 12
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-13.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-13.disk_size_gb}"
    create_option = "Attach"
    lun           = 13
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-14.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-14.disk_size_gb}"
    create_option = "Attach"
    lun           = 14
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-05-datadisk-15.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-05-datadisk-15.disk_size_gb}"
    create_option = "Attach"
    lun           = 15
    caching       = "ReadWrite"
  }
}

### nfs-file-06
resource "azurerm_public_ip" "nfs-file-06-public-ip1" {
  name                         = "nfs-file-06-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-06"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-06" {
  name                    = "nfs-file-06"
  internal_dns_name_label = "nfs-file-06"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-06-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.106"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-06-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-06-datadisk-0" {
  name                 = "file-06-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-1" {
  name                 = "file-06-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-2" {
  name                 = "file-06-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-3" {
  name                 = "file-06-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-4" {
  name                 = "file-06-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-5" {
  name                 = "file-06-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-6" {
  name                 = "file-06-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-7" {
  name                 = "file-06-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-8" {
  name                 = "file-06-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-9" {
  name                 = "file-06-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-10" {
  name                 = "file-06-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-11" {
  name                 = "file-06-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-12" {
  name                 = "file-06-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-13" {
  name                 = "file-06-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-14" {
  name                 = "file-06-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-06-datadisk-15" {
  name                 = "file-06-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-06" {
  name                          = "nfs-file-06"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-06.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-06"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-06"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-0.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-0.disk_size_gb}"
    create_option = "Attach"
    lun           = 0
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-1.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-1.disk_size_gb}"
    create_option = "Attach"
    lun           = 1
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-2.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-2.disk_size_gb}"
    create_option = "Attach"
    lun           = 2
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-3.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-3.disk_size_gb}"
    create_option = "Attach"
    lun           = 3
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-4.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-4.disk_size_gb}"
    create_option = "Attach"
    lun           = 4
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-5.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-5.disk_size_gb}"
    create_option = "Attach"
    lun           = 5
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-6.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-6.disk_size_gb}"
    create_option = "Attach"
    lun           = 6
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-7.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-7.disk_size_gb}"
    create_option = "Attach"
    lun           = 7
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-8.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-8.disk_size_gb}"
    create_option = "Attach"
    lun           = 8
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-9.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-9.disk_size_gb}"
    create_option = "Attach"
    lun           = 9
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-10.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-10.disk_size_gb}"
    create_option = "Attach"
    lun           = 10
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-11.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-11.disk_size_gb}"
    create_option = "Attach"
    lun           = 11
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-12.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-12.disk_size_gb}"
    create_option = "Attach"
    lun           = 12
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-13.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-13.disk_size_gb}"
    create_option = "Attach"
    lun           = 13
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-14.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-14.disk_size_gb}"
    create_option = "Attach"
    lun           = 14
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-06-datadisk-15.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-06-datadisk-15.disk_size_gb}"
    create_option = "Attach"
    lun           = 15
    caching       = "ReadWrite"
  }
}

### nfs-file-07
resource "azurerm_public_ip" "nfs-file-07-public-ip1" {
  name                         = "nfs-file-07-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-07"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-07" {
  name                    = "nfs-file-07"
  internal_dns_name_label = "nfs-file-07"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-07-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.107"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-07-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-07-datadisk-0" {
  name                 = "file-07-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-1" {
  name                 = "file-07-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-2" {
  name                 = "file-07-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-3" {
  name                 = "file-07-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-4" {
  name                 = "file-07-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-5" {
  name                 = "file-07-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-6" {
  name                 = "file-07-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-7" {
  name                 = "file-07-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-8" {
  name                 = "file-07-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-9" {
  name                 = "file-07-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-10" {
  name                 = "file-07-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-11" {
  name                 = "file-07-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-12" {
  name                 = "file-07-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-13" {
  name                 = "file-07-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-14" {
  name                 = "file-07-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-07-datadisk-15" {
  name                 = "file-07-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-07" {
  name                          = "nfs-file-07"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-07.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-07"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-07"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-0.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-0.disk_size_gb}"
    create_option = "Attach"
    lun           = 0
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-1.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-1.disk_size_gb}"
    create_option = "Attach"
    lun           = 1
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-2.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-2.disk_size_gb}"
    create_option = "Attach"
    lun           = 2
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-3.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-3.disk_size_gb}"
    create_option = "Attach"
    lun           = 3
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-4.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-4.disk_size_gb}"
    create_option = "Attach"
    lun           = 4
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-5.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-5.disk_size_gb}"
    create_option = "Attach"
    lun           = 5
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-7.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-7.disk_size_gb}"
    create_option = "Attach"
    lun           = 7
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-6.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-6.disk_size_gb}"
    create_option = "Attach"
    lun           = 6
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-8.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-8.disk_size_gb}"
    create_option = "Attach"
    lun           = 8
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-9.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-9.disk_size_gb}"
    create_option = "Attach"
    lun           = 9
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-10.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-10.disk_size_gb}"
    create_option = "Attach"
    lun           = 10
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-11.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-11.disk_size_gb}"
    create_option = "Attach"
    lun           = 11
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-12.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-12.disk_size_gb}"
    create_option = "Attach"
    lun           = 12
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-13.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-13.disk_size_gb}"
    create_option = "Attach"
    lun           = 13
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-14.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-14.disk_size_gb}"
    create_option = "Attach"
    lun           = 14
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-07-datadisk-15.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-07-datadisk-15.disk_size_gb}"
    create_option = "Attach"
    lun           = 15
    caching       = "ReadWrite"
  }
}

### nfs-file-08
resource "azurerm_public_ip" "nfs-file-08-public-ip1" {
  name                         = "nfs-file-08-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-08"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-08" {
  name                    = "nfs-file-08"
  internal_dns_name_label = "nfs-file-08"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-08-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.108"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-08-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-08-datadisk-0" {
  name                 = "file-08-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-1" {
  name                 = "file-08-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-2" {
  name                 = "file-08-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-3" {
  name                 = "file-08-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-4" {
  name                 = "file-08-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-5" {
  name                 = "file-08-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-6" {
  name                 = "file-08-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-7" {
  name                 = "file-08-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-8" {
  name                 = "file-08-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-9" {
  name                 = "file-08-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-10" {
  name                 = "file-08-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-11" {
  name                 = "file-08-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-12" {
  name                 = "file-08-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-13" {
  name                 = "file-08-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-14" {
  name                 = "file-08-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-08-datadisk-15" {
  name                 = "file-08-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-08" {
  name                          = "nfs-file-08"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-08.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-08"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-08"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-0.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-0.disk_size_gb}"
    create_option = "Attach"
    lun           = 0
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-1.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-1.disk_size_gb}"
    create_option = "Attach"
    lun           = 1
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-2.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-2.disk_size_gb}"
    create_option = "Attach"
    lun           = 2
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-3.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-3.disk_size_gb}"
    create_option = "Attach"
    lun           = 3
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-4.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-4.disk_size_gb}"
    create_option = "Attach"
    lun           = 4
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-5.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-5.disk_size_gb}"
    create_option = "Attach"
    lun           = 5
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-6.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-6.disk_size_gb}"
    create_option = "Attach"
    lun           = 6
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-7.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-7.disk_size_gb}"
    create_option = "Attach"
    lun           = 7
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-8.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-8.disk_size_gb}"
    create_option = "Attach"
    lun           = 8
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-9.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-9.disk_size_gb}"
    create_option = "Attach"
    lun           = 9
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-10.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-10.disk_size_gb}"
    create_option = "Attach"
    lun           = 10
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-11.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-11.disk_size_gb}"
    create_option = "Attach"
    lun           = 11
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-12.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-12.disk_size_gb}"
    create_option = "Attach"
    lun           = 12
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-13.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-13.disk_size_gb}"
    create_option = "Attach"
    lun           = 13
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-14.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-14.disk_size_gb}"
    create_option = "Attach"
    lun           = 14
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.file-08-datadisk-15.name}"
    disk_size_gb  = "${azurerm_managed_disk.file-08-datadisk-15.disk_size_gb}"
    create_option = "Attach"
    lun           = 15
    caching       = "ReadWrite"
  }
}

### nfs-file-09
resource "azurerm_public_ip" "nfs-file-09-public-ip1" {
  name                         = "nfs-file-09-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-09"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-09" {
  name                    = "nfs-file-09"
  internal_dns_name_label = "nfs-file-09"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-09-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.109"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-09-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-09-datadisk-0" {
  name                 = "file-09-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-1" {
  name                 = "file-09-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-2" {
  name                 = "file-09-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-3" {
  name                 = "file-09-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-4" {
  name                 = "file-09-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-5" {
  name                 = "file-09-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-6" {
  name                 = "file-09-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-7" {
  name                 = "file-09-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-8" {
  name                 = "file-09-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-9" {
  name                 = "file-09-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-10" {
  name                 = "file-09-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-11" {
  name                 = "file-09-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-12" {
  name                 = "file-09-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-13" {
  name                 = "file-09-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-14" {
  name                 = "file-09-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-09-datadisk-15" {
  name                 = "file-09-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-09" {
  name                          = "nfs-file-09"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-09.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-09"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-09"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-09-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.file-09-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-09-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### nfs-file-10
resource "azurerm_public_ip" "nfs-file-10-public-ip1" {
  name                         = "nfs-file-10-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-10"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-10" {
  name                    = "nfs-file-10"
  internal_dns_name_label = "nfs-file-10"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-10-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.110"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-10-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-10-datadisk-0" {
  name                 = "file-10-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-1" {
  name                 = "file-10-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-2" {
  name                 = "file-10-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-3" {
  name                 = "file-10-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-4" {
  name                 = "file-10-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-5" {
  name                 = "file-10-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-6" {
  name                 = "file-10-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-7" {
  name                 = "file-10-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-8" {
  name                 = "file-10-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-9" {
  name                 = "file-10-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-10" {
  name                 = "file-10-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-11" {
  name                 = "file-10-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-12" {
  name                 = "file-10-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-13" {
  name                 = "file-10-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-14" {
  name                 = "file-10-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-10-datadisk-15" {
  name                 = "file-10-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-10" {
  name                          = "nfs-file-10"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-10.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-10"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-10"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-10-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.file-10-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-10-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### nfs-file-11
resource "azurerm_public_ip" "nfs-file-11-public-ip1" {
  name                         = "nfs-file-11-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-11"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-11" {
  name                    = "nfs-file-11"
  internal_dns_name_label = "nfs-file-11"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-11-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.111"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-11-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-11-datadisk-0" {
  name                 = "file-11-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-1" {
  name                 = "file-11-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-2" {
  name                 = "file-11-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-3" {
  name                 = "file-11-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-4" {
  name                 = "file-11-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-5" {
  name                 = "file-11-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-6" {
  name                 = "file-11-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-7" {
  name                 = "file-11-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-8" {
  name                 = "file-11-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-9" {
  name                 = "file-11-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-10" {
  name                 = "file-11-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-11" {
  name                 = "file-11-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-12" {
  name                 = "file-11-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-13" {
  name                 = "file-11-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-14" {
  name                 = "file-11-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-11-datadisk-15" {
  name                 = "file-11-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-11" {
  name                          = "nfs-file-11"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-11.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-11"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-11"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-11-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.file-11-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-11-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### nfs-file-12
resource "azurerm_public_ip" "nfs-file-12-public-ip1" {
  name                         = "nfs-file-12-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-12"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-12" {
  name                    = "nfs-file-12"
  internal_dns_name_label = "nfs-file-12"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-12-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.112"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-12-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-12-datadisk-0" {
  name                 = "file-12-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-1" {
  name                 = "file-12-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-2" {
  name                 = "file-12-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-3" {
  name                 = "file-12-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-4" {
  name                 = "file-12-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-5" {
  name                 = "file-12-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-6" {
  name                 = "file-12-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-7" {
  name                 = "file-12-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-8" {
  name                 = "file-12-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-9" {
  name                 = "file-12-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-10" {
  name                 = "file-12-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-11" {
  name                 = "file-12-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-12" {
  name                 = "file-12-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-13" {
  name                 = "file-12-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-14" {
  name                 = "file-12-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-12-datadisk-15" {
  name                 = "file-12-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-12" {
  name                          = "nfs-file-12"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-12.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-12"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-12"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-12-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.file-12-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-12-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### nfs-file-13
resource "azurerm_public_ip" "nfs-file-13-public-ip1" {
  name                         = "nfs-file-13-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-13"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-13" {
  name                    = "nfs-file-13"
  internal_dns_name_label = "nfs-file-13"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-13-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.113"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-13-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-13-datadisk-0" {
  name                 = "file-13-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-1" {
  name                 = "file-13-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-2" {
  name                 = "file-13-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-3" {
  name                 = "file-13-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-4" {
  name                 = "file-13-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-5" {
  name                 = "file-13-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-6" {
  name                 = "file-13-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-7" {
  name                 = "file-13-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-8" {
  name                 = "file-13-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-9" {
  name                 = "file-13-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-10" {
  name                 = "file-13-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-11" {
  name                 = "file-13-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-12" {
  name                 = "file-13-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-13" {
  name                 = "file-13-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-14" {
  name                 = "file-13-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-13-datadisk-15" {
  name                 = "file-13-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-13" {
  name                          = "nfs-file-13"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-13.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-13"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-13"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-13-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.file-13-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-13-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### nfs-file-14
resource "azurerm_public_ip" "nfs-file-14-public-ip1" {
  name                         = "nfs-file-14-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-14"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-14" {
  name                    = "nfs-file-14"
  internal_dns_name_label = "nfs-file-14"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-14-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.114"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-14-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-14-datadisk-0" {
  name                 = "file-14-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-1" {
  name                 = "file-14-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-2" {
  name                 = "file-14-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-3" {
  name                 = "file-14-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-4" {
  name                 = "file-14-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-5" {
  name                 = "file-14-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-6" {
  name                 = "file-14-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-7" {
  name                 = "file-14-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-8" {
  name                 = "file-14-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-9" {
  name                 = "file-14-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-10" {
  name                 = "file-14-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-11" {
  name                 = "file-14-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-12" {
  name                 = "file-14-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-13" {
  name                 = "file-14-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-14" {
  name                 = "file-14-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-14-datadisk-15" {
  name                 = "file-14-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-14" {
  name                          = "nfs-file-14"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-14.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-14"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-14"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-14-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.file-14-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-14-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### nfs-file-15
resource "azurerm_public_ip" "nfs-file-15-public-ip1" {
  name                         = "nfs-file-15-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-15"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-15" {
  name                    = "nfs-file-15"
  internal_dns_name_label = "nfs-file-15"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-15-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.115"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-15-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-15-datadisk-0" {
  name                 = "file-15-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-1" {
  name                 = "file-15-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-2" {
  name                 = "file-15-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-3" {
  name                 = "file-15-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-4" {
  name                 = "file-15-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-5" {
  name                 = "file-15-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-6" {
  name                 = "file-15-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-7" {
  name                 = "file-15-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-8" {
  name                 = "file-15-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-9" {
  name                 = "file-15-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-10" {
  name                 = "file-15-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-11" {
  name                 = "file-15-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-12" {
  name                 = "file-15-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-13" {
  name                 = "file-15-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-14" {
  name                 = "file-15-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-15-datadisk-15" {
  name                 = "file-15-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-15" {
  name                          = "nfs-file-15"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-15.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-15"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-15"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-15-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.file-15-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-15-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### nfs-file-16
resource "azurerm_public_ip" "nfs-file-16-public-ip1" {
  name                         = "nfs-file-16-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-16"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-16" {
  name                    = "nfs-file-16"
  internal_dns_name_label = "nfs-file-16"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-16-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.116"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-16-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-16-datadisk-0" {
  name                 = "file-16-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-1" {
  name                 = "file-16-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-2" {
  name                 = "file-16-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-3" {
  name                 = "file-16-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-4" {
  name                 = "file-16-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-5" {
  name                 = "file-16-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-6" {
  name                 = "file-16-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-7" {
  name                 = "file-16-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-8" {
  name                 = "file-16-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-9" {
  name                 = "file-16-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-10" {
  name                 = "file-16-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-11" {
  name                 = "file-16-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-12" {
  name                 = "file-16-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-13" {
  name                 = "file-16-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-14" {
  name                 = "file-16-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-16-datadisk-15" {
  name                 = "file-16-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-16" {
  name                          = "nfs-file-16"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-16.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-16"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-16"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-16-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.file-16-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-16-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### nfs-file-17
resource "azurerm_public_ip" "nfs-file-17-public-ip1" {
  name                         = "nfs-file-17-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-17"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-17" {
  name                    = "nfs-file-17"
  internal_dns_name_label = "nfs-file-17"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-17-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.117"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-17-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-17-datadisk-0" {
  name                 = "file-17-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-1" {
  name                 = "file-17-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-2" {
  name                 = "file-17-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-3" {
  name                 = "file-17-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-4" {
  name                 = "file-17-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-5" {
  name                 = "file-17-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-6" {
  name                 = "file-17-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-7" {
  name                 = "file-17-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-8" {
  name                 = "file-17-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-9" {
  name                 = "file-17-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-10" {
  name                 = "file-17-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-11" {
  name                 = "file-17-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-12" {
  name                 = "file-17-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-13" {
  name                 = "file-17-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-14" {
  name                 = "file-17-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-17-datadisk-15" {
  name                 = "file-17-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-17" {
  name                          = "nfs-file-17"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-17.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-17"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-17"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-17-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.file-17-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-17-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### nfs-file-18
resource "azurerm_public_ip" "nfs-file-18-public-ip1" {
  name                         = "nfs-file-18-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-18"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-18" {
  name                    = "nfs-file-18"
  internal_dns_name_label = "nfs-file-18"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-18-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.118"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-18-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-18-datadisk-0" {
  name                 = "file-18-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-1" {
  name                 = "file-18-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-2" {
  name                 = "file-18-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-3" {
  name                 = "file-18-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-4" {
  name                 = "file-18-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-5" {
  name                 = "file-18-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-6" {
  name                 = "file-18-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-7" {
  name                 = "file-18-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-8" {
  name                 = "file-18-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-9" {
  name                 = "file-18-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-10" {
  name                 = "file-18-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-11" {
  name                 = "file-18-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-12" {
  name                 = "file-18-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-13" {
  name                 = "file-18-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-14" {
  name                 = "file-18-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-18-datadisk-15" {
  name                 = "file-18-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-18" {
  name                          = "nfs-file-18"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-18.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-18"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-18"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-18-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.file-18-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-18-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### nfs-file-19
resource "azurerm_public_ip" "nfs-file-19-public-ip1" {
  name                         = "nfs-file-19-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-19"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-19" {
  name                    = "nfs-file-19"
  internal_dns_name_label = "nfs-file-19"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-19-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.119"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-19-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-19-datadisk-0" {
  name                 = "file-19-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-1" {
  name                 = "file-19-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-2" {
  name                 = "file-19-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-3" {
  name                 = "file-19-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-4" {
  name                 = "file-19-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-5" {
  name                 = "file-19-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-6" {
  name                 = "file-19-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-7" {
  name                 = "file-19-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-8" {
  name                 = "file-19-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-9" {
  name                 = "file-19-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-10" {
  name                 = "file-19-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-11" {
  name                 = "file-19-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-12" {
  name                 = "file-19-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-13" {
  name                 = "file-19-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-14" {
  name                 = "file-19-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-19-datadisk-15" {
  name                 = "file-19-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-19" {
  name                          = "nfs-file-19"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-19.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-19"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-19"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-19-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.file-19-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-19-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### nfs-file-20
resource "azurerm_public_ip" "nfs-file-20-public-ip1" {
  name                         = "nfs-file-20-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-file-20"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-file-20" {
  name                    = "nfs-file-20"
  internal_dns_name_label = "nfs-file-20"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-file-20-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.120"
    public_ip_address_id          = "${azurerm_public_ip.nfs-file-20-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "file-20-datadisk-0" {
  name                 = "file-20-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-1" {
  name                 = "file-20-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-2" {
  name                 = "file-20-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-3" {
  name                 = "file-20-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-4" {
  name                 = "file-20-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-5" {
  name                 = "file-20-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-6" {
  name                 = "file-20-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-7" {
  name                 = "file-20-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-8" {
  name                 = "file-20-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-9" {
  name                 = "file-20-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-10" {
  name                 = "file-20-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-11" {
  name                 = "file-20-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-12" {
  name                 = "file-20-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-13" {
  name                 = "file-20-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-14" {
  name                 = "file-20-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "file-20-datadisk-15" {
  name                 = "file-20-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-file-20" {
  name                          = "nfs-file-20"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-file-20.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-file-20"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-file-20"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.file-20-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.file-20-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.file-20-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### nfs-lfs-01
resource "azurerm_public_ip" "nfs-lfs-01-public-ip1" {
  name                         = "nfs-lfs-01-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-lfs-01"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-lfs-01" {
  name                    = "nfs-lfs-01"
  internal_dns_name_label = "nfs-lfs-01"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-lfs-01-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.121"
    public_ip_address_id          = "${azurerm_public_ip.nfs-lfs-01-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "lfs-01-datadisk-0" {
  name                 = "lfs-01-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-1" {
  name                 = "lfs-01-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-2" {
  name                 = "lfs-01-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-3" {
  name                 = "lfs-01-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-4" {
  name                 = "lfs-01-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-5" {
  name                 = "lfs-01-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-6" {
  name                 = "lfs-01-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-7" {
  name                 = "lfs-01-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-8" {
  name                 = "lfs-01-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-9" {
  name                 = "lfs-01-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-10" {
  name                 = "lfs-01-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-11" {
  name                 = "lfs-01-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-12" {
  name                 = "lfs-01-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-13" {
  name                 = "lfs-01-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-14" {
  name                 = "lfs-01-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "lfs-01-datadisk-15" {
  name                 = "lfs-01-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-lfs-01" {
  name                          = "nfs-lfs-01"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-lfs-01.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-lfs-01"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-lfs-01"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-0.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-0.disk_size_gb}"
    create_option = "Attach"
    lun           = 0
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-1.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-1.disk_size_gb}"
    create_option = "Attach"
    lun           = 1
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-2.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-2.disk_size_gb}"
    create_option = "Attach"
    lun           = 2
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-3.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-3.disk_size_gb}"
    create_option = "Attach"
    lun           = 3
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-4.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-4.disk_size_gb}"
    create_option = "Attach"
    lun           = 4
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-5.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-5.disk_size_gb}"
    create_option = "Attach"
    lun           = 5
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-6.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-6.disk_size_gb}"
    create_option = "Attach"
    lun           = 6
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-7.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-7.disk_size_gb}"
    create_option = "Attach"
    lun           = 7
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-8.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-8.disk_size_gb}"
    create_option = "Attach"
    lun           = 8
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-9.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-9.disk_size_gb}"
    create_option = "Attach"
    lun           = 9
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-10.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-10.disk_size_gb}"
    create_option = "Attach"
    lun           = 10
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-11.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-11.disk_size_gb}"
    create_option = "Attach"
    lun           = 11
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-12.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-12.disk_size_gb}"
    create_option = "Attach"
    lun           = 12
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-13.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-13.disk_size_gb}"
    create_option = "Attach"
    lun           = 13
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-14.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-14.disk_size_gb}"
    create_option = "Attach"
    lun           = 14
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.lfs-01-datadisk-15.name}"
    disk_size_gb  = "${azurerm_managed_disk.lfs-01-datadisk-15.disk_size_gb}"
    create_option = "Attach"
    lun           = 15
    caching       = "ReadWrite"
  }
}

### nfs-share-01
resource "azurerm_public_ip" "nfs-share-01-public-ip1" {
  name                         = "nfs-share-01-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-share-01"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-share-01" {
  name                    = "nfs-share-01"
  internal_dns_name_label = "nfs-share-01"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-share-01-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.131"
    public_ip_address_id          = "${azurerm_public_ip.nfs-share-01-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "share-01-datadisk-0" {
  name                 = "share-01-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-1" {
  name                 = "share-01-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-2" {
  name                 = "share-01-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-3" {
  name                 = "share-01-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-4" {
  name                 = "share-01-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-5" {
  name                 = "share-01-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-6" {
  name                 = "share-01-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-7" {
  name                 = "share-01-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-8" {
  name                 = "share-01-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-9" {
  name                 = "share-01-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-10" {
  name                 = "share-01-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-11" {
  name                 = "share-01-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-12" {
  name                 = "share-01-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-13" {
  name                 = "share-01-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-14" {
  name                 = "share-01-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "share-01-datadisk-15" {
  name                 = "share-01-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-share-01" {
  name                          = "nfs-share-01"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-share-01.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-share-01"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-share-01"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-0.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-0.disk_size_gb}"
    create_option = "Attach"
    lun           = 0
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-1.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-1.disk_size_gb}"
    create_option = "Attach"
    lun           = 1
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-2.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-2.disk_size_gb}"
    create_option = "Attach"
    lun           = 2
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-3.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-3.disk_size_gb}"
    create_option = "Attach"
    lun           = 3
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-4.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-4.disk_size_gb}"
    create_option = "Attach"
    lun           = 4
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-5.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-5.disk_size_gb}"
    create_option = "Attach"
    lun           = 5
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-6.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-6.disk_size_gb}"
    create_option = "Attach"
    lun           = 6
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-7.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-7.disk_size_gb}"
    create_option = "Attach"
    lun           = 7
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-8.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-8.disk_size_gb}"
    create_option = "Attach"
    lun           = 8
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-9.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-9.disk_size_gb}"
    create_option = "Attach"
    lun           = 9
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-10.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-10.disk_size_gb}"
    create_option = "Attach"
    lun           = 10
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-11.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-11.disk_size_gb}"
    create_option = "Attach"
    lun           = 11
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-12.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-12.disk_size_gb}"
    create_option = "Attach"
    lun           = 12
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-13.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-13.disk_size_gb}"
    create_option = "Attach"
    lun           = 13
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-14.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-14.disk_size_gb}"
    create_option = "Attach"
    lun           = 14
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.share-01-datadisk-15.name}"
    disk_size_gb  = "${azurerm_managed_disk.share-01-datadisk-15.disk_size_gb}"
    create_option = "Attach"
    lun           = 15
    caching       = "ReadWrite"
  }
}

### nfs-uploads-01
resource "azurerm_public_ip" "nfs-uploads-01-public-ip1" {
  name                         = "nfs-uploads-01-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-uploads-01"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-uploads-01" {
  name                    = "nfs-uploads-01"
  internal_dns_name_label = "nfs-uploads-01"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-uploads-01-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.141"
    public_ip_address_id          = "${azurerm_public_ip.nfs-uploads-01-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "uploads-01-datadisk-0" {
  name                 = "uploads-01-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-1" {
  name                 = "uploads-01-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-2" {
  name                 = "uploads-01-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-3" {
  name                 = "uploads-01-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-4" {
  name                 = "uploads-01-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-5" {
  name                 = "uploads-01-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-6" {
  name                 = "uploads-01-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-7" {
  name                 = "uploads-01-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-8" {
  name                 = "uploads-01-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-9" {
  name                 = "uploads-01-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-10" {
  name                 = "uploads-01-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-11" {
  name                 = "uploads-01-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-12" {
  name                 = "uploads-01-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-13" {
  name                 = "uploads-01-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-14" {
  name                 = "uploads-01-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "uploads-01-datadisk-15" {
  name                 = "uploads-01-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-uploads-01" {
  name                          = "nfs-uploads-01"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-uploads-01.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-uploads-01"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-uploads-01"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-0.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-0.disk_size_gb}"
    create_option = "Attach"
    lun           = 0
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-1.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-1.disk_size_gb}"
    create_option = "Attach"
    lun           = 1
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-2.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-2.disk_size_gb}"
    create_option = "Attach"
    lun           = 2
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-3.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-3.disk_size_gb}"
    create_option = "Attach"
    lun           = 3
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-4.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-4.disk_size_gb}"
    create_option = "Attach"
    lun           = 4
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-5.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-5.disk_size_gb}"
    create_option = "Attach"
    lun           = 5
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-6.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-6.disk_size_gb}"
    create_option = "Attach"
    lun           = 6
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-7.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-7.disk_size_gb}"
    create_option = "Attach"
    lun           = 7
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-8.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-8.disk_size_gb}"
    create_option = "Attach"
    lun           = 8
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-9.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-9.disk_size_gb}"
    create_option = "Attach"
    lun           = 9
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-10.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-10.disk_size_gb}"
    create_option = "Attach"
    lun           = 10
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-11.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-11.disk_size_gb}"
    create_option = "Attach"
    lun           = 11
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-12.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-12.disk_size_gb}"
    create_option = "Attach"
    lun           = 12
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-13.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-13.disk_size_gb}"
    create_option = "Attach"
    lun           = 13
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-14.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-14.disk_size_gb}"
    create_option = "Attach"
    lun           = 14
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.uploads-01-datadisk-15.name}"
    disk_size_gb  = "${azurerm_managed_disk.uploads-01-datadisk-15.disk_size_gb}"
    create_option = "Attach"
    lun           = 15
    caching       = "ReadWrite"
  }
}

### nfs-pages-01
resource "azurerm_public_ip" "nfs-pages-01-public-ip1" {
  name                         = "nfs-pages-01-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-pages-01"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-pages-01" {
  name                    = "nfs-pages-01"
  internal_dns_name_label = "nfs-pages-01"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-pages-01-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.161"
    public_ip_address_id          = "${azurerm_public_ip.nfs-pages-01-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "pages-01-datadisk-0" {
  name                 = "pages-01-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-1" {
  name                 = "pages-01-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-2" {
  name                 = "pages-01-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-3" {
  name                 = "pages-01-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-4" {
  name                 = "pages-01-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-5" {
  name                 = "pages-01-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-6" {
  name                 = "pages-01-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-7" {
  name                 = "pages-01-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-8" {
  name                 = "pages-01-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-9" {
  name                 = "pages-01-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-10" {
  name                 = "pages-01-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-11" {
  name                 = "pages-01-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-12" {
  name                 = "pages-01-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-13" {
  name                 = "pages-01-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-14" {
  name                 = "pages-01-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "pages-01-datadisk-15" {
  name                 = "pages-01-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-pages-01" {
  name                          = "nfs-pages-01"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-pages-01.id}"]
  vm_size                       = "Standard_DS13_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-pages-01"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-pages-01"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-0.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-0.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-0.id}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-1.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-1.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-1.id}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-2.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-2.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-2.id}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-3.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-3.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-3.id}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-4.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-4.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-4.id}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-5.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-5.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-5.id}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-6.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-6.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-6.id}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-7.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-7.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-7.id}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-8.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-8.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-8.id}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-9.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-9.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-9.id}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-10.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-10.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-10.id}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-11.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-11.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-11.id}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-12.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-12.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-12.id}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-13.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-13.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-13.id}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-14.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-14.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-14.id}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.pages-01-datadisk-15.name}"
    disk_size_gb    = "${azurerm_managed_disk.pages-01-datadisk-15.disk_size_gb}"
    managed_disk_id = "${azurerm_managed_disk.pages-01-datadisk-15.id}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }
}

### nfs-artifacts-01
resource "azurerm_public_ip" "nfs-artifacts-01-public-ip1" {
  name                         = "nfs-artifacts-01-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-artifacts-01"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-artifacts-01" {
  name                    = "nfs-artifacts-01"
  internal_dns_name_label = "nfs-artifacts-01"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-artifacts-01-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.151"
    public_ip_address_id          = "${azurerm_public_ip.nfs-artifacts-01-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-0" {
  name                 = "artifacts-01-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-1" {
  name                 = "artifacts-01-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-2" {
  name                 = "artifacts-01-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-3" {
  name                 = "artifacts-01-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-4" {
  name                 = "artifacts-01-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-5" {
  name                 = "artifacts-01-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-6" {
  name                 = "artifacts-01-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-7" {
  name                 = "artifacts-01-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-8" {
  name                 = "artifacts-01-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-9" {
  name                 = "artifacts-01-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-10" {
  name                 = "artifacts-01-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-11" {
  name                 = "artifacts-01-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-12" {
  name                 = "artifacts-01-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-13" {
  name                 = "artifacts-01-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-14" {
  name                 = "artifacts-01-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-15" {
  name                 = "artifacts-01-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-16" {
  name                 = "artifacts-01-datadisk-16"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-17" {
  name                 = "artifacts-01-datadisk-17"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-18" {
  name                 = "artifacts-01-datadisk-18"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-19" {
  name                 = "artifacts-01-datadisk-19"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-20" {
  name                 = "artifacts-01-datadisk-20"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-21" {
  name                 = "artifacts-01-datadisk-21"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-22" {
  name                 = "artifacts-01-datadisk-22"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-23" {
  name                 = "artifacts-01-datadisk-23"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-24" {
  name                 = "artifacts-01-datadisk-24"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-25" {
  name                 = "artifacts-01-datadisk-25"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-26" {
  name                 = "artifacts-01-datadisk-26"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-27" {
  name                 = "artifacts-01-datadisk-27"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-28" {
  name                 = "artifacts-01-datadisk-28"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "artifacts-01-datadisk-29" {
  name                 = "artifacts-01-datadisk-29"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Import"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-artifacts-01" {
  name                          = "nfs-artifacts-01"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-artifacts-01.id}"]
  vm_size                       = "Standard_DS14_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-artifacts-01"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-artifacts-01"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-0.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-0.disk_size_gb}"
    create_option = "Attach"
    lun           = 0
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-1.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-1.disk_size_gb}"
    create_option = "Attach"
    lun           = 1
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-2.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-2.disk_size_gb}"
    create_option = "Attach"
    lun           = 2
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-3.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-3.disk_size_gb}"
    create_option = "Attach"
    lun           = 3
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-4.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-4.disk_size_gb}"
    create_option = "Attach"
    lun           = 4
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-5.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-5.disk_size_gb}"
    create_option = "Attach"
    lun           = 5
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-6.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-6.disk_size_gb}"
    create_option = "Attach"
    lun           = 6
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-7.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-7.disk_size_gb}"
    create_option = "Attach"
    lun           = 7
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-8.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-8.disk_size_gb}"
    create_option = "Attach"
    lun           = 8
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-9.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-9.disk_size_gb}"
    create_option = "Attach"
    lun           = 9
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-10.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-10.disk_size_gb}"
    create_option = "Attach"
    lun           = 10
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-11.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-11.disk_size_gb}"
    create_option = "Attach"
    lun           = 11
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-12.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-12.disk_size_gb}"
    create_option = "Attach"
    lun           = 12
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-13.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-13.disk_size_gb}"
    create_option = "Attach"
    lun           = 13
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-14.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-14.disk_size_gb}"
    create_option = "Attach"
    lun           = 14
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-15.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-15.disk_size_gb}"
    create_option = "Attach"
    lun           = 15
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-16.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-16.disk_size_gb}"
    create_option = "Attach"
    lun           = 16
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-17.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-17.disk_size_gb}"
    create_option = "Attach"
    lun           = 17
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-18.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-18.disk_size_gb}"
    create_option = "Attach"
    lun           = 18
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-19.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-19.disk_size_gb}"
    create_option = "Attach"
    lun           = 19
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-20.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-20.disk_size_gb}"
    create_option = "Attach"
    lun           = 20
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-21.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-21.disk_size_gb}"
    create_option = "Attach"
    lun           = 21
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-22.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-22.disk_size_gb}"
    create_option = "Attach"
    lun           = 22
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-23.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-23.disk_size_gb}"
    create_option = "Attach"
    lun           = 23
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-24.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-24.disk_size_gb}"
    create_option = "Attach"
    lun           = 24
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-25.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-25.disk_size_gb}"
    create_option = "Attach"
    lun           = 25
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-26.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-26.disk_size_gb}"
    create_option = "Attach"
    lun           = 26
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-27.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-27.disk_size_gb}"
    create_option = "Attach"
    lun           = 27
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-28.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-28.disk_size_gb}"
    create_option = "Attach"
    lun           = 28
    caching       = "ReadWrite"
  }

  storage_data_disk {
    name          = "${azurerm_managed_disk.artifacts-01-datadisk-29.name}"
    disk_size_gb  = "${azurerm_managed_disk.artifacts-01-datadisk-29.disk_size_gb}"
    create_option = "Attach"
    lun           = 29
    caching       = "ReadWrite"
  }
}

### nfs-flake-01 (replacement for file-storage1)

resource "azurerm_public_ip" "nfs-flake-01-public-ip1" {
  name                         = "nfs-flake-01-public-ip"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
  domain_name_label            = "nfs-flake-01"
  idle_timeout_in_minutes      = 30
}

resource "azurerm_network_interface" "nfs-flake-01" {
  name                    = "nfs-flake-01"
  internal_dns_name_label = "nfs-flake-01"
  location                = "${var.location}"
  resource_group_name     = "${var.resource_group_name}"

  ip_configuration {
    name                          = "nfs-flake-01-ip-configuration"
    subnet_id                     = "${var.subnet_id}"
    private_ip_address_allocation = "static"
    private_ip_address            = "10.70.2.171"
    public_ip_address_id          = "${azurerm_public_ip.nfs-flake-01-public-ip1.id}"
  }
}

resource "azurerm_managed_disk" "flake-01-datadisk-0" {
  name                 = "flake-01-datadisk-0"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-1" {
  name                 = "flake-01-datadisk-1"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-2" {
  name                 = "flake-01-datadisk-2"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-3" {
  name                 = "flake-01-datadisk-3"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-4" {
  name                 = "flake-01-datadisk-4"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-5" {
  name                 = "flake-01-datadisk-5"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-6" {
  name                 = "flake-01-datadisk-6"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-7" {
  name                 = "flake-01-datadisk-7"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-8" {
  name                 = "flake-01-datadisk-8"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-9" {
  name                 = "flake-01-datadisk-9"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-10" {
  name                 = "flake-01-datadisk-10"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-11" {
  name                 = "flake-01-datadisk-11"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-12" {
  name                 = "flake-01-datadisk-12"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-13" {
  name                 = "flake-01-datadisk-13"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-14" {
  name                 = "flake-01-datadisk-14"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-15" {
  name                 = "flake-01-datadisk-15"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-16" {
  name                 = "flake-01-datadisk-16"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-17" {
  name                 = "flake-01-datadisk-17"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-18" {
  name                 = "flake-01-datadisk-18"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-19" {
  name                 = "flake-01-datadisk-19"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-20" {
  name                 = "flake-01-datadisk-20"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-21" {
  name                 = "flake-01-datadisk-21"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-22" {
  name                 = "flake-01-datadisk-22"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-23" {
  name                 = "flake-01-datadisk-23"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-24" {
  name                 = "flake-01-datadisk-24"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-25" {
  name                 = "flake-01-datadisk-25"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-26" {
  name                 = "flake-01-datadisk-26"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-27" {
  name                 = "flake-01-datadisk-27"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-28" {
  name                 = "flake-01-datadisk-28"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-29" {
  name                 = "flake-01-datadisk-29"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-30" {
  name                 = "flake-01-datadisk-30"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_managed_disk" "flake-01-datadisk-31" {
  name                 = "flake-01-datadisk-31"
  location             = "East US 2"
  resource_group_name  = "${var.resource_group_name}"
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "1023"
}

resource "azurerm_virtual_machine" "nfs-flake-01" {
  name                          = "nfs-flake-01"
  location                      = "${var.location}"
  resource_group_name           = "${var.resource_group_name}"
  network_interface_ids         = ["${azurerm_network_interface.nfs-flake-01.id}"]
  vm_size                       = "Standard_DS14_v2"
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "16.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "osdisk-nfs-flake-01"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  os_profile {
    computer_name  = "nfs-flake-01"
    admin_username = "${var.first_user_username}"
    admin_password = "${var.first_user_password}"

    # We will use custom_data to bootstrap the instance
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-0.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-0.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-0.disk_size_gb}"
    create_option   = "Attach"
    lun             = 0
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-1.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-1.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-1.disk_size_gb}"
    create_option   = "Attach"
    lun             = 1
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-2.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-2.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-2.disk_size_gb}"
    create_option   = "Attach"
    lun             = 2
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-3.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-3.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-3.disk_size_gb}"
    create_option   = "Attach"
    lun             = 3
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-4.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-4.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-4.disk_size_gb}"
    create_option   = "Attach"
    lun             = 4
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-5.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-5.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-5.disk_size_gb}"
    create_option   = "Attach"
    lun             = 5
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-6.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-6.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-6.disk_size_gb}"
    create_option   = "Attach"
    lun             = 6
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-7.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-7.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-7.disk_size_gb}"
    create_option   = "Attach"
    lun             = 7
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-8.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-8.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-8.disk_size_gb}"
    create_option   = "Attach"
    lun             = 8
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-9.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-9.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-9.disk_size_gb}"
    create_option   = "Attach"
    lun             = 9
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-10.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-10.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-10.disk_size_gb}"
    create_option   = "Attach"
    lun             = 10
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-11.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-11.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-11.disk_size_gb}"
    create_option   = "Attach"
    lun             = 11
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-12.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-12.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-12.disk_size_gb}"
    create_option   = "Attach"
    lun             = 12
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-13.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-13.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-13.disk_size_gb}"
    create_option   = "Attach"
    lun             = 13
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-14.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-14.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-14.disk_size_gb}"
    create_option   = "Attach"
    lun             = 14
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-15.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-15.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-15.disk_size_gb}"
    create_option   = "Attach"
    lun             = 15
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-16.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-16.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-16.disk_size_gb}"
    create_option   = "Attach"
    lun             = 16
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-17.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-17.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-17.disk_size_gb}"
    create_option   = "Attach"
    lun             = 17
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-18.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-18.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-18.disk_size_gb}"
    create_option   = "Attach"
    lun             = 18
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-19.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-19.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-19.disk_size_gb}"
    create_option   = "Attach"
    lun             = 19
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-20.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-20.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-20.disk_size_gb}"
    create_option   = "Attach"
    lun             = 20
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-21.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-21.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-21.disk_size_gb}"
    create_option   = "Attach"
    lun             = 21
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-22.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-22.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-22.disk_size_gb}"
    create_option   = "Attach"
    lun             = 22
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-23.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-23.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-23.disk_size_gb}"
    create_option   = "Attach"
    lun             = 23
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-24.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-24.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-24.disk_size_gb}"
    create_option   = "Attach"
    lun             = 24
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-25.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-25.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-25.disk_size_gb}"
    create_option   = "Attach"
    lun             = 25
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-26.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-26.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-26.disk_size_gb}"
    create_option   = "Attach"
    lun             = 26
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-27.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-27.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-27.disk_size_gb}"
    create_option   = "Attach"
    lun             = 27
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-28.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-28.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-28.disk_size_gb}"
    create_option   = "Attach"
    lun             = 28
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-29.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-29.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-29.disk_size_gb}"
    create_option   = "Attach"
    lun             = 29
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-30.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-30.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-30.disk_size_gb}"
    create_option   = "Attach"
    lun             = 30
    caching         = "ReadWrite"
  }

  storage_data_disk {
    name            = "${azurerm_managed_disk.flake-01-datadisk-31.name}"
    managed_disk_id = "${azurerm_managed_disk.flake-01-datadisk-31.id}"
    disk_size_gb    = "${azurerm_managed_disk.flake-01-datadisk-31.disk_size_gb}"
    create_option   = "Attach"
    lun             = 31
    caching         = "ReadWrite"
  }
}
