variable "location" {}
variable "resource_group_name" {}
variable "subnet_id" {}
variable "first_user_username" {}
variable "first_user_password" {}
variable "chef_repo_dir" {}
variable "chef_vaults" {}

variable "chef_vault_env" {
  default = "_default"
}

variable "gitlab_com_zone_id" {}
variable "frontend_lb_count" {}
variable "pages_lb_count" {}
variable "altssh_lb_count" {}
variable "frontend_lb_pool" {}
variable "pages_lb_pool" {}
variable "altssh_lb_pool" {}
