resource "azurerm_public_ip" "HAProdLB-Pages" {
  name                         = "HAProdLB-Pages"
  location                     = "${var.location}"
  resource_group_name          = "${var.resource_group_name}"
  public_ip_address_allocation = "static"
}

resource "azurerm_lb" "PagesLBProd" {
  name                = "PagesLBProd"
  location            = "${var.location}"
  resource_group_name = "${var.resource_group_name}"

  frontend_ip_configuration {
    name                 = "PagesLBProdIPConfiguration"
    public_ip_address_id = "${azurerm_public_ip.HAProdLB-Pages.id}"
  }
}

resource "azurerm_lb_backend_address_pool" "PagesLBProd" {
  resource_group_name = "${var.resource_group_name}"
  loadbalancer_id     = "${azurerm_lb.PagesLBProd.id}"
  name                = "PagesProdPool"
}

resource "azurerm_lb_probe" "pages-http" {
  resource_group_name = "${var.resource_group_name}"
  loadbalancer_id     = "${azurerm_lb.PagesLBProd.id}"
  name                = "http"
  port                = 80
  protocol            = "Tcp"
  interval_in_seconds = 5
  number_of_probes    = 2
}

resource "azurerm_lb_rule" "pages-http" {
  resource_group_name            = "${var.resource_group_name}"
  loadbalancer_id                = "${azurerm_lb.PagesLBProd.id}"
  name                           = "http"
  protocol                       = "Tcp"
  frontend_ip_configuration_name = "PagesLBProdIPConfiguration"
  frontend_port                  = 80
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.PagesLBProd.id}"
  backend_port                   = 80
  load_distribution              = "SourceIPProtocol"
  probe_id                       = "${azurerm_lb_probe.pages-http.id}"
}

resource "azurerm_lb_probe" "pages-https" {
  resource_group_name = "${var.resource_group_name}"
  loadbalancer_id     = "${azurerm_lb.PagesLBProd.id}"
  name                = "https"
  port                = 443
  protocol            = "Tcp"
  interval_in_seconds = 5
  number_of_probes    = 2
}

resource "azurerm_lb_rule" "pages-https" {
  resource_group_name            = "${var.resource_group_name}"
  loadbalancer_id                = "${azurerm_lb.PagesLBProd.id}"
  name                           = "https"
  protocol                       = "Tcp"
  frontend_ip_configuration_name = "PagesLBProdIPConfiguration"
  frontend_port                  = 443
  backend_address_pool_id        = "${azurerm_lb_backend_address_pool.PagesLBProd.id}"
  backend_port                   = 443
  load_distribution              = "SourceIPProtocol"
  probe_id                       = "${azurerm_lb_probe.pages-https.id}"
}
