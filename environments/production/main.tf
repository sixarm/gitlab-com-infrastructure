variable "environment" {
  default = "prod"
}

## Azure

variable "arm_subscription_id" {}
variable "arm_client_id" {}
variable "arm_client_secret" {}
variable "arm_tenant_id" {}

# We need these variables as part of the virtual machine creation.
# These will go away as soon as we switch to pre-baked server images.
# - Daniele

variable "first_user_username" {}
variable "first_user_password" {}

# These are the new variables to connect to the newly created instance, which
# replace the two above.

variable "ssh_user" {}
variable "ssh_private_key" {}
variable "ssh_public_key" {}

variable "location" {
  default = "East US 2"
}

provider "azurerm" {
  subscription_id = "${var.arm_subscription_id}"
  client_id       = "${var.arm_client_id}"
  client_secret   = "${var.arm_client_secret}"
  tenant_id       = "${var.arm_tenant_id}"
}

## Chef
variable "chef_version" {
  default = "12.19.36"
}

variable "chef_repo_dir" {}

## AWS
provider "aws" {
  region = "us-east-1"
}

variable "gitlab_com_zone_id" {}
variable "gitlab_net_zone_id" {}

terraform {
  backend "s3" {}
}

### Vnet

module "vnet" {
  source        = "vnet"
  location      = "${var.location}"
  address_space = ["10.64.0.0/11"]
}

### Subnets

module "subnet-external-lb" {
  source              = "subnets/external-lb"
  location            = "${var.location}"
  subnet_cidr         = "10.65.1.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-internal-lb" {
  source              = "subnets/internal-lb"
  location            = "${var.location}"
  subnet_cidr         = "10.65.2.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-postgres" {
  source              = "subnets/postgres"
  location            = "${var.location}"
  subnet_cidr         = "10.66.1.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-redis" {
  source              = "subnets/redis"
  location            = "${var.location}"
  subnet_cidr         = "10.66.2.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-elasticsearch" {
  source              = "subnets/elasticsearch"
  location            = "${var.location}"
  subnet_cidr         = "10.66.3.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-pgbouncer" {
  source              = "subnets/pgbouncer"
  location            = "${var.location}"
  subnet_cidr         = "10.66.4.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-consul" {
  source              = "subnets/consul"
  location            = "${var.location}"
  subnet_cidr         = "10.67.1.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-vault" {
  source              = "subnets/vault"
  location            = "${var.location}"
  subnet_cidr         = "10.67.2.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-deploy" {
  source              = "subnets/deploy"
  location            = "${var.location}"
  subnet_cidr         = "10.67.3.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-dns" {
  source              = "subnets/dns"
  location            = "${var.location}"
  subnet_cidr         = "10.67.4.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-monitoring" {
  source              = "subnets/monitoring"
  location            = "${var.location}"
  subnet_cidr         = "10.68.1.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-log" {
  source              = "subnets/log"
  location            = "${var.location}"
  subnet_cidr         = "10.68.2.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-log-storage" {
  source              = "subnets/log-storage"
  location            = "${var.location}"
  subnet_cidr         = "10.68.50.0/24"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-api" {
  source              = "subnets/api"
  location            = "${var.location}"
  subnet_cidr         = "10.69.2.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-git" {
  source              = "subnets/git"
  location            = "${var.location}"
  subnet_cidr         = "10.69.4.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-sidekiq" {
  source              = "subnets/sidekiq"
  location            = "${var.location}"
  subnet_cidr         = "10.69.6.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-web" {
  source              = "subnets/web"
  location            = "${var.location}"
  subnet_cidr         = "10.69.8.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-registry" {
  source              = "subnets/registry"
  location            = "${var.location}"
  subnet_cidr         = "10.69.10.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-mailroom" {
  source              = "subnets/mailroom"
  location            = "${var.location}"
  subnet_cidr         = "10.69.14.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

module "subnet-storage" {
  source              = "subnets/storage"
  location            = "${var.location}"
  subnet_cidr         = "10.70.2.0/23"
  vnet_name           = "${module.vnet.name}"
  vnet_resource_group = "${module.vnet.resource_group_name}"
}

### Load Balancers

module "load-balancers-external-lb" {
  source              = "load-balancers/external-lb"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-external-lb.resource_group_name}"
}

// module "load-balancers-internal" {
//   source                          = "load-balancers/internal"
//   location                        = "${var.location}"
//   resource_group_name             = "${module.subnet-internal-lb.resource_group_name}"
// }

### Virtual Machines

module "virtual-machines-prometheus" {
  node_count          = 2
  name                = "prometheus-app"
  source              = "virtual-machines/prometheus"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-monitoring.resource_group_name}"
  subnet_id           = "${module.subnet-monitoring.subnet_id}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  gitlab_net_zone_id  = "${var.gitlab_net_zone_id}"
}

module "virtual-machines-external-lb" {
  frontend_lb_count   = 6
  pages_lb_count      = 2
  altssh_lb_count     = 2
  source              = "virtual-machines/external-lb"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-external-lb.resource_group_name}"
  subnet_id           = "${module.subnet-external-lb.subnet_id}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\", \"gitlab_cluster_lb\": \"_default\"}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  altssh_lb_pool      = "${module.load-balancers-external-lb.altssh_backend_pool_id}"
  frontend_lb_pool    = "${module.load-balancers-external-lb.frontend_backend_pool_id}"
  pages_lb_pool       = "${module.load-balancers-external-lb.pages_backend_pool_id}"
}

// module "virtual-machines-internal-lb" {
//   source                          = "virtual-machines/internal-lb"
//   location                        = "${var.location}"
//   resource_group_name             = "${module.subnet-internal-lb.resource_group_name}"
//   subnet_id                       = "${module.subnet-internal-lb.subnet_id}"
//   first_user_username             = "${var.first_user_username}"
//   first_user_password             = "${var.first_user_password}"
//   chef_repo_dir                   = "${var.chef_repo_dir}"
//   chef_vaults                     = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\"}"
//   gitlab_com_zone_id              = "${var.gitlab_com_zone_id}"
//   load_balancer_backend_address_pool_id = "${module.load-balancers-internal.backend_pool_id}"
// }

module "virtual-machines-postgres" {
  address_prefix      = "${module.subnet-postgres.address_prefix}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{ \"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\", \"gitlab-monitor\": \"prd\", \"postgres-exporter\": \"prd\" }"
  chef_version        = "${var.chef_version}"
  count               = 4
  environment         = "${var.environment}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  instance_type       = "Standard_GS5"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-postgres.resource_group_name}"
  source              = "virtual-machines/postgres"
  ssh_private_key     = "${var.ssh_private_key}"
  ssh_public_key      = "${var.ssh_public_key}"
  ssh_user            = "${var.ssh_user}"
  subnet_id           = "${module.subnet-postgres.subnet_id}"
  tier                = "db"
}

module "virtual-machines-pgbouncer" {
  address_prefix      = "${module.subnet-pgbouncer.address_prefix}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"prd_client\", \"gitlab-monitor\": \"_default\", \"postgres-exporter\": \"_default\"}"
  chef_version        = "${var.chef_version}"
  count               = 2
  environment         = "${var.environment}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  instance_type       = "Standard_A1_v2"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-pgbouncer.resource_group_name}"
  source              = "../../modules/virtual-machines/pgbouncer"
  ssh_private_key     = "${var.ssh_private_key}"
  ssh_public_key      = "${var.ssh_public_key}"
  ssh_user            = "${var.ssh_user}"
  subnet_id           = "${module.subnet-pgbouncer.subnet_id}"
  tier                = "db"
}

module "virtual-machines-redis" {
  count               = 3
  source              = "virtual-machines/redis"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-redis.resource_group_name}"
  subnet_id           = "${module.subnet-redis.subnet_id}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\"}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
}

module "virtual-machines-redis-cache" {
  address_prefix         = "${module.subnet-redis.address_prefix}"
  chef_repo_dir          = "${var.chef_repo_dir}"
  chef_vaults            = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\"}"
  chef_version           = "${var.chef_version}"
  environment            = "${var.environment}"
  gitlab_com_zone_id     = "${var.gitlab_com_zone_id}"
  ip_skew                = 10
  location               = "${var.location}"
  persistence            = "cache"
  redis_count            = 3
  redis_instance_type    = "Standard_DS14_v2"
  resource_group_name    = "${module.subnet-redis.resource_group_name}"
  sentinel_count         = 3
  sentinel_datadisk_size = 100
  sentinel_instance_type = "Standard_A1_v2"
  source                 = "../../modules/virtual-machines/redis"
  ssh_private_key        = "${var.ssh_private_key}"
  ssh_public_key         = "${var.ssh_public_key}"
  ssh_user               = "${var.ssh_user}"
  subnet_id              = "${module.subnet-redis.subnet_id}"
  tier                   = "db"
}

module "virtual-machines-elasticsearch" {
  count               = 0
  source              = "../../modules/virtual-machines/elasticsearch"
  resource_group_name = "${module.subnet-elasticsearch.resource_group_name}"
  subnet_id           = "${module.subnet-elasticsearch.subnet_id}"
  instance_type       = "Standard_DS13_v2"
  tier                = "db"
  environment         = "${var.environment}"
  address_prefix      = "${module.subnet-elasticsearch.address_prefix}"
  location            = "${var.location}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\"}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
}

module "virtual-machines-logsearch" {
  address_prefix                   = "${module.subnet-log-storage.address_prefix}"
  chef_repo_dir                    = "${var.chef_repo_dir}"
  chef_vaults                      = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\"}"
  chef_version                     = "${var.chef_version}"
  environment                      = "${var.environment}"
  gitlab_net_zone_id               = "${var.gitlab_net_zone_id}"
  location                         = "${var.location}"
  resource_group_name              = "${module.subnet-log-storage.resource_group_name}"
  logsearch_client_count           = 3
  logsearch_client_instance_type   = "Standard_E8_v3"
  logsearch_datanode_count         = 5
  logsearch_datanode_instance_type = "Standard_E8s_v3"
  logsearch_master_count           = 3
  logsearch_master_instance_type   = "Standard_E4_v3"
  source                           = "../../modules/virtual-machines/logsearch"
  ssh_private_key                  = "${var.ssh_private_key}"
  ssh_public_key                   = "${var.ssh_public_key}"
  ssh_user                         = "${var.ssh_user}"
  subnet_id                        = "${module.subnet-log-storage.subnet_id}"
  tier                             = "log"
}

module "virtual-machines-deploy" {
  source              = "virtual-machines/deploy"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-deploy.resource_group_name}"
  subnet_id           = "${module.subnet-deploy.subnet_id}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\"}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
}

module "virtual-machines-dns" {
  count               = 3
  instance_type       = "Standard_A2_v2"
  source              = "../../modules/virtual-machines/dns"
  resource_group_name = "${module.subnet-dns.resource_group_name}"
  subnet_id           = "${module.subnet-dns.subnet_id}"
  tier                = "inf"
  environment         = "${var.environment}"
  address_prefix      = "${module.subnet-dns.address_prefix}"
  location            = "${var.location}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\"}"
  gitlab_net_zone_id  = "${var.gitlab_net_zone_id}"
}

module "virtual-machines-consul" {
  count               = 3
  instance_type       = "Standard_A2_v2"
  source              = "../../modules/virtual-machines/consul"
  resource_group_name = "${module.subnet-consul.resource_group_name}"
  subnet_id           = "${module.subnet-consul.subnet_id}"
  tier                = "inf"
  environment         = "${var.environment}"
  address_prefix      = "${module.subnet-consul.address_prefix}"
  location            = "${var.location}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\"}"
  gitlab_net_zone_id  = "${var.gitlab_net_zone_id}"
}

module "virtual-machines-api" {
  address_prefix      = "${module.subnet-api.address_prefix}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\"}"
  chef_version        = "${var.chef_version}"
  count               = 20
  environment         = "${var.environment}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  instance_type       = "Standard_DS14"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-api.resource_group_name}"
  source              = "../../modules/virtual-machines/api"
  ssh_private_key     = "${var.ssh_private_key}"
  ssh_public_key      = "${var.ssh_public_key}"
  ssh_user            = "${var.ssh_user}"
  subnet_id           = "${module.subnet-api.subnet_id}"
  tier                = "sv"
}

module "virtual-machines-git" {
  address_prefix      = "${module.subnet-git.address_prefix}"
  chef_repo_dir       = "${var.chef_repo_dir}"
  chef_vaults         = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\"}"
  chef_version        = "${var.chef_version}"
  count               = 12
  environment         = "${var.environment}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  instance_type       = "Standard_F16s"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-git.resource_group_name}"
  source              = "../../modules/virtual-machines/git"
  ssh_private_key     = "${var.ssh_private_key}"
  ssh_public_key      = "${var.ssh_public_key}"
  ssh_user            = "${var.ssh_user}"
  subnet_id           = "${module.subnet-git.subnet_id}"
  tier                = "sv"
}

module "virtual-machines-sidekiq" {
  address_prefix                      = "${module.subnet-sidekiq.address_prefix}"
  chef_repo_dir                       = "${var.chef_repo_dir}"
  chef_vaults                         = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\"}"
  chef_version                        = "${var.chef_version}"
  environment                         = "${var.environment}"
  first_user_password                 = "${var.first_user_password}"
  first_user_username                 = "${var.first_user_username}"
  gitlab_com_zone_id                  = "${var.gitlab_com_zone_id}"
  location                            = "${var.location}"
  resource_group_name                 = "${module.subnet-sidekiq.resource_group_name}"
  sidekiq_asap_count                  = 5
  sidekiq_asap_instance_type          = "Standard_D3_v2"
  sidekiq_besteffort_count            = 3
  sidekiq_besteffort_instance_type    = "Standard_D8_v3"
  sidekiq_elasticsearch_count         = 0
  sidekiq_elasticsearch_instance_type = "Standard_A4_v2"
  sidekiq_pages_count                 = 6
  sidekiq_pages_instance_type         = "Standard_D2"
  sidekiq_pipeline_count              = 3
  sidekiq_pipeline_instance_type      = "Standard_A4_v2"
  sidekiq_pullmirror_count            = 7
  sidekiq_pullmirror_instance_type    = "Standard_D2_v3"
  sidekiq_realtime_count              = 4
  sidekiq_realtime_instance_type      = "Standard_D3_v2"
  sidekiq_traces_count                = 5
  sidekiq_traces_instance_type        = "Standard_A8_v2"
  source                              = "../../modules/virtual-machines/sidekiq"
  ssh_private_key                     = "${var.ssh_private_key}"
  ssh_public_key                      = "${var.ssh_public_key}"
  ssh_user                            = "${var.ssh_user}"
  subnet_id                           = "${module.subnet-sidekiq.subnet_id}"
  tier                                = "sv"
}

module "virtual-machines-web" {
  address_prefix = "${module.subnet-web.address_prefix}"
  chef_repo_dir  = "${var.chef_repo_dir}"

  chef_vaults         = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\"}"
  chef_version        = "${var.chef_version}"
  count               = 14
  environment         = "${var.environment}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  instance_type       = "Standard_F16s"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-web.resource_group_name}"
  source              = "../../modules/virtual-machines/web"
  ssh_private_key     = "${var.ssh_private_key}"
  ssh_public_key      = "${var.ssh_public_key}"
  ssh_user            = "${var.ssh_user}"
  subnet_id           = "${module.subnet-web.subnet_id}"
  tier                = "sv"
}

module "virtual-machines-registry" {
  count               = 2
  source              = "../../modules/virtual-machines/registry"
  resource_group_name = "${module.subnet-registry.resource_group_name}"
  subnet_id           = "${module.subnet-registry.subnet_id}"
  instance_type       = "Standard_A2_v2"
  tier                = "sv"
  environment         = "${var.environment}"
  address_prefix      = "${module.subnet-registry.address_prefix}"
  location            = "${var.location}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  chef_repo_dir       = "${var.chef_repo_dir}"

  chef_vaults        = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\"}"
  gitlab_com_zone_id = "${var.gitlab_com_zone_id}"
}

module "virtual-machines-mailroom" {
  address_prefix = "${module.subnet-mailroom.address_prefix}"
  chef_repo_dir  = "${var.chef_repo_dir}"

  chef_vaults         = "{\"syslog_client\": \"prd\", \"gitlab-cluster-base\": \"prd\", \"gitlab_consul\": \"client\"}"
  chef_version        = "${var.chef_version}"
  count               = 2
  environment         = "${var.environment}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
  instance_type       = "Standard_D3_v2"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-mailroom.resource_group_name}"
  source              = "../../modules/virtual-machines/mailroom"
  ssh_private_key     = "${var.ssh_private_key}"
  ssh_public_key      = "${var.ssh_public_key}"
  ssh_user            = "${var.ssh_user}"
  subnet_id           = "${module.subnet-mailroom.subnet_id}"
  tier                = "sv"
}

module "virtual-machines-storage" {
  source              = "virtual-machines/storage"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-storage.resource_group_name}"
  subnet_id           = "${module.subnet-storage.subnet_id}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  gitlab_com_zone_id  = "${var.gitlab_com_zone_id}"
}

module "virtual-machines-monitoring" {
  source              = "virtual-machines/monitoring"
  location            = "${var.location}"
  resource_group_name = "${module.subnet-monitoring.resource_group_name}"
  subnet_id           = "${module.subnet-monitoring.subnet_id}"
  first_user_username = "${var.first_user_username}"
  first_user_password = "${var.first_user_password}"
  gitlab_net_zone_id  = "${var.gitlab_net_zone_id}"
}
