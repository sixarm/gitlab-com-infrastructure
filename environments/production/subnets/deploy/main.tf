variable "location" {
  description = "The location"
}

variable "vnet_name" {
  description = "The name of the virtual network"
}

variable "vnet_resource_group" {
  description = "The name of the virtual network"
}

variable "subnet_cidr" {
  description = "The CIDR of the subnet"
}

resource "azurerm_resource_group" "DeployProd" {
  name     = "DeployProd"
  location = "${var.location}"
}

resource "azurerm_network_security_group" "DeployProd" {
  name                = "DeployProd"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.DeployProd.name}"
}

resource "azurerm_network_security_rule" "ssh-from-vpn1-ext" {
  name                        = "ssh-from-vpn1-ext"
  priority                    = 146
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "52.177.194.133"
  destination_port_range      = "22"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.DeployProd.name}"
  network_security_group_name = "${azurerm_network_security_group.DeployProd.name}"
}

resource "azurerm_network_security_rule" "ssh-from-vpn2-ext" {
  name                        = "ssh-from-vpn2-ext"
  priority                    = 147
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "52.177.192.239"
  destination_port_range      = "22"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.DeployProd.name}"
  network_security_group_name = "${azurerm_network_security_group.DeployProd.name}"
}

resource "azurerm_network_security_rule" "ssh-from-internal" {
  name                        = "ssh-from-internal"
  priority                    = 148
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "10.0.0.0/8"
  destination_port_range      = "22"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.DeployProd.name}"
  network_security_group_name = "${azurerm_network_security_group.DeployProd.name}"
}

resource "azurerm_network_security_rule" "ssh-from-vpn" {
  name                        = "ssh-from-vpn"
  priority                    = 149
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "10.254.4.0/23"
  destination_port_range      = "22"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.DeployProd.name}"
  network_security_group_name = "${azurerm_network_security_group.DeployProd.name}"
}

resource "azurerm_network_security_rule" "ssh" {
  name                        = "ssh"
  priority                    = 150
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "Internet"
  destination_port_range      = "22"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.DeployProd.name}"
  network_security_group_name = "${azurerm_network_security_group.DeployProd.name}"
}

resource "azurerm_subnet" "DeployProd" {
  name                      = "DeployProd"
  resource_group_name       = "${var.vnet_resource_group}"
  virtual_network_name      = "${var.vnet_name}"
  address_prefix            = "${var.subnet_cidr}"
  network_security_group_id = "${azurerm_network_security_group.DeployProd.id}"
}

output "subnet_id" {
  value = "${azurerm_subnet.DeployProd.id}"
}

output "address_prefix" {
  value = "${azurerm_subnet.DeployProd.address_prefix}"
}

output "resource_group_name" {
  value = "DeployProd"
}

output "resource_group_id" {
  value = "${azurerm_resource_group.DeployProd.id}"
}
