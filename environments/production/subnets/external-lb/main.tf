variable "location" {
  description = "The location"
}

variable "vnet_name" {
  description = "The name of the virtual network"
}

variable "vnet_resource_group" {
  description = "The name of the virtual network"
}

variable "subnet_cidr" {
  description = "The CIDR of the subnet"
}

resource "azurerm_resource_group" "ExternalLBProd" {
  name     = "ExternalLBProd"
  location = "${var.location}"
}

resource "azurerm_network_security_group" "ExternalLBProd" {
  name                = "ExternalLBProd"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.ExternalLBProd.name}"
}

resource "azurerm_network_security_rule" "https" {
  name                        = "https"
  priority                    = 145
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "Internet"
  destination_port_range      = "443"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.ExternalLBProd.name}"
  network_security_group_name = "${azurerm_network_security_group.ExternalLBProd.name}"
}

resource "azurerm_network_security_rule" "ssh" {
  name                        = "ssh"
  priority                    = 150
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "Internet"
  destination_port_range      = "22"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.ExternalLBProd.name}"
  network_security_group_name = "${azurerm_network_security_group.ExternalLBProd.name}"
}

resource "azurerm_network_security_rule" "ssh-2222-from-vpn1-ext" {
  name                        = "ssh-2222-from-vpn1-ext"
  priority                    = 160
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "52.177.194.133"
  destination_port_range      = "2222"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.ExternalLBProd.name}"
  network_security_group_name = "${azurerm_network_security_group.ExternalLBProd.name}"
}

resource "azurerm_network_security_rule" "ssh-2222-from-vpn2-ext" {
  name                        = "ssh-2222-from-vpn2-ext"
  priority                    = 161
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "52.177.192.239"
  destination_port_range      = "2222"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.ExternalLBProd.name}"
  network_security_group_name = "${azurerm_network_security_group.ExternalLBProd.name}"
}

resource "azurerm_network_security_rule" "ssh-2222-from-internal" {
  name                        = "ssh-2222-from-internal"
  priority                    = 162
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "10.0.0.0/8"
  destination_port_range      = "2222"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.ExternalLBProd.name}"
  network_security_group_name = "${azurerm_network_security_group.ExternalLBProd.name}"
}

resource "azurerm_network_security_rule" "ssh-2222-from-vpn" {
  name                        = "ssh-2222-from-vpn"
  priority                    = 163
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "10.254.4.0/23"
  destination_port_range      = "2222"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.ExternalLBProd.name}"
  network_security_group_name = "${azurerm_network_security_group.ExternalLBProd.name}"
}

resource "azurerm_network_security_rule" "ssh-2222-deny" {
  name                        = "ssh-2222-deny"
  priority                    = 164
  direction                   = "Inbound"
  access                      = "Deny"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "Internet"
  destination_port_range      = "2222"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.ExternalLBProd.name}"
  network_security_group_name = "${azurerm_network_security_group.ExternalLBProd.name}"
}

resource "azurerm_network_security_rule" "prometheus" {
  name                        = "prometheus"
  priority                    = 151
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "10.4.1.0/24"
  destination_port_range      = "9100"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.ExternalLBProd.name}"
  network_security_group_name = "${azurerm_network_security_group.ExternalLBProd.name}"
}

resource "azurerm_network_security_rule" "http" {
  name                        = "http"
  priority                    = 140
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "TCP"
  source_port_range           = "*"
  source_address_prefix       = "Internet"
  destination_port_range      = "80"
  destination_address_prefix  = "*"
  resource_group_name         = "${azurerm_resource_group.ExternalLBProd.name}"
  network_security_group_name = "${azurerm_network_security_group.ExternalLBProd.name}"
}

resource "azurerm_subnet" "ExternalLBProd" {
  name                      = "ExternalLBProd"
  resource_group_name       = "${var.vnet_resource_group}"
  virtual_network_name      = "${var.vnet_name}"
  address_prefix            = "${var.subnet_cidr}"
  network_security_group_id = "${azurerm_network_security_group.ExternalLBProd.id}"
}

output "subnet_id" {
  value = "${azurerm_subnet.ExternalLBProd.id}"
}

output "address_prefix" {
  value = "${azurerm_subnet.ExternalLBProd.address_prefix}"
}

output "resource_group_name" {
  value = "ExternalLBProd"
}

output "resource_group_id" {
  value = "${azurerm_resource_group.ExternalLBProd.id}"
}
