variable "location" {
  description = "The location"
}

variable "virtual_network_cidr" {
  description = "The CIDR of the virtual network"
}

resource "azurerm_resource_group" "GitLabCanary" {
  name     = "GitLabCanary"
  location = "${var.location}"
}

resource "azurerm_virtual_network" "GitLabCanary" {
  name                = "GitLabCanary"
  address_space       = ["${var.virtual_network_cidr}"]
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.GitLabCanary.name}"
}

resource "azurerm_network_security_group" "gitlab-Canary-nsg" {
  name                = "gitlab-Canary-nsg"
  location            = "${var.location}"
  resource_group_name = "${azurerm_resource_group.GitLabCanary.name}"

  security_rule {
    name                       = "default-allow-ssh"
    priority                   = 1000
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "TCP"
    source_port_range          = "*"
    source_address_prefix      = "*"
    destination_port_range     = "22"
    destination_address_prefix = "*"
  }
}

output "id" {
  value = "${azurerm_virtual_network.GitLabCanary.id}"
}

output "name" {
  value = "${azurerm_virtual_network.GitLabCanary.name}"
}

output "security_group_id" {
  value = "${azurerm_network_security_group.gitlab-Canary-nsg.id}"
}

output "resource_group_name" {
  value = "${azurerm_resource_group.GitLabCanary.name}"
}
