// Configure remote state
terraform {
  backend "s3" {}
}

// Use credentials from environment or shared credentials file
provider "aws" {
  region  = "us-east-1"
  version = "~> 1.41"
}

module "cloudtrail" {
  source                        = "git::https://github.com/cloudposse/terraform-aws-cloudtrail.git?ref=0.5.0"
  namespace                     = "gitlab"
  stage                         = "aws"
  name                          = "cloudtrail-default"
  enable_logging                = "true"
  enable_log_file_validation    = "true"
  include_global_service_events = "true"
  is_multi_region_trail         = "true"
  s3_bucket_name                = "${module.cloudtrail_s3_bucket.bucket_id}"
}

module "cloudtrail_s3_bucket" {
  source    = "git::https://github.com/cloudposse/terraform-aws-cloudtrail-s3-bucket.git?ref=0.1.1"
  namespace = "gitlab"
  stage     = "aws"
  name      = "cloudtrail-default"
  region    = "us-east-1"
}
